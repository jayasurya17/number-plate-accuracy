from difflib import SequenceMatcher

json = {
    0 : ["O","Q",9,"D"],
    1 : ["L"],
    2 : ["Z"],
    5 : ["S"],
    8 : ["B","R"],
    9 : ["0"],
    "B" : [8,"R"],
    "D" : [0,"O","Q"],
    "H" : ["M"],
    "L" : [1],
    "M" : ["H","N","W"],
    "N" : ["M","H"],
    "O" : [0,"Q","D"],
    "Q" : [0,"O","D"],
    "R" : [8,"B"],
    "S" : [5]
}

def validate(number, original, max):
    count = 0
    for digit in number:
        count = count + 1
        for key in json:
            if str(key) == str(digit):
                for value in json[key]:
                    temp_value = number[:count-1] + str(value) + number[count:]
                    s = SequenceMatcher(None, str(temp_value), str(original))
                    print str(temp_value) + " : " +  str(s.ratio())
                    if max > 9:
                        validate(str(temp_value), str(original), max - 1)

input = "KA S0 K 3DB7"
original = "KA 50 K 3087"
print "ORIGINAL number : "+str(original)
print ""
print "INPUT from algorithm : "+str(input)
print ""
validate(input,original,len(input) - 1)
